import { Controller, Get, NotFoundException, Param, ParseIntPipe } from '@nestjs/common';

import { EmployeeService } from './employee.service';

@Controller('employee')
export class EmployeeController {
    constructor(private readonly employeeService: EmployeeService) {}

    @Get(':id')
    async getAllEmployees(@Param('id', ParseIntPipe) id: number) {
        try {
            return await this.employeeService.getEmployee(id);
        }  catch(error) {
            throw new NotFoundException();
        }
    }
}
