import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';

import { Employee } from './entity/emolpyee.entity';

@Injectable()
export class EmployeeService {
    constructor(
        @InjectRepository(Employee) 
        private readonly employeeRepository: Repository<Employee>,
    ){}

    // Get One Employee
    async getEmployee(id: number) {
        const employee = await this.employeeRepository.findOneBy({ id });
        
        if(!employee) {
            throw new Error('Employee not found');
        }

        return employee;
    }
}
